FROM java:8
EXPOSE 8090:8090
ADD /target/springdocker-demo.jar springdocker-demo.jar
ENTRYPOINT ["java","-jar","springdocker-demo.jar"]